var db = db.getSiblingDB('dev');

db.address.insert([
  {
    zipCode: 13598468,
    address: 'address info 13598468',
    neighborhood: 'neighborhood info 13598468',
    city: 'city info 13598468',
    state: 'state info 13598468'
  },
  {
    zipCode: 13598460,
    address: 'address info 13598460',
    neighborhood: 'neighborhood info 13598460',
    city: 'city info 13598460',
    state: 'state info 13598460'
  },
  {
    zipCode: 13598400,
    address: 'address info 13598400',
    neighborhood: 'neighborhood info 13598400',
    city: 'city info 13598400',
    state: 'state info 13598400'
  },
  {
    zipCode: 13598000,
    address: 'address info 13598000',
    neighborhood: 'neighborhood info 13598000',
    city: 'city info 13598000',
    state: 'state info 13598000'
  },
  {
    zipCode: 10000000,
    address: 'address info 10000000',
    neighborhood: 'neighborhood info 10000000',
    city: 'city info 10000000',
    state: 'state info 10000000'
  },
  {
    zipCode: 5880432,
    address: 'address info 5881432',
    neighborhood: 'neighborhood info 5881432',
    city: 'city info 5881432',
    state: 'state info 5881432'
  },
  {
    zipCode: 5880430,
    address: 'address info 5881430',
    neighborhood: 'neighborhood info 5881430',
    city: 'city info 5881430',
    state: 'state info 5881430'
  },
  {
    zipCode: 5880400,
    address: 'address info 5880400',
    neighborhood: 'neighborhood info 5880400',
    city: 'city info 5880400',
    state: 'state info 5880400'
  },
  {
    zipCode: 5880000,
    address: 'address info 5880000',
    neighborhood: 'neighborhood info 5880000',
    city: 'city info 5880000',
    state: 'state info 5880000'
  }
]);

db.address.createIndex({ 'zipCode': 1 });