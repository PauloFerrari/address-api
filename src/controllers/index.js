const address = require('./v1/address-controller');
const authentication = require('./v1/authentication-controller');
const healthcheck = require('./v1/healthcheck-controller');

module.exports = {
  address,
  authentication,
  healthcheck
};