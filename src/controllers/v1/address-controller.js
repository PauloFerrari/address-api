const constants = require('../../constants');
const { ValidationError, NotFoundError } = require('../../errors');
const Parameters = require('../../parameters');

class AddressController {
  static async getByZipCode(req, res, next) {
    const parameters = new Parameters.address.Get(req);

    const parametersErros = parameters.validate();

    if (parametersErros.length > 0) {
      throw new ValidationError(parametersErros);
    }

    const address = await req.repository.findAddressByZipCode(constants.collections.address, parameters.zipCodes);

    if (address === null) {
      throw new NotFoundError(constants.errors.zipCode.notFound);
    }

    res.json(address);

    return next();
  }
}

module.exports = AddressController;