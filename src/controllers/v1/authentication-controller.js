const constants = require('../../constants');
const { ValidationError, Unauthorized } = require('../../errors');
const Parameters = require('../../parameters');
const services = require('../../services');

class AuthenticationController {
  static async generateToken(req, res, next) {
    const parameters = new Parameters.authentication.GetToken(req);

    const parametersErros = parameters.validate();

    if (parametersErros.length > 0) {
      throw new ValidationError(parametersErros);
    }

    const client = await req.repository.findOne(constants.collections.clients, parameters.client);

    if (client === null) {
      throw new Unauthorized();
    }

    const token = services.authentication.genareteToken(client);

    res.json({ token });

    return next();
  }

  static async verifyToken(req, res, next) {
    res.status(204).end();

    return next();
  }
}

module.exports = AuthenticationController;