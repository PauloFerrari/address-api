const moment = require('moment');
const constants = require('../../constants');
const logger = require('../../logger');
const packageJson = require('../../../package.json');
const services = require('../../services');
const { ServiceUnavailableError } = require('../../errors');

class HealthcheckController {
  static async get(req, res, next) {
    const dbConnection = await services.database.getConnection(constants.collections.admin);

    try {
      await dbConnection.admin().ping();
    } catch (error) {
      logger.error(error);
      throw new ServiceUnavailableError();
    }

    res.json({
      mongo: true,
      requestCount: req.requestCount,
      uptime: _getUpTime(req.startServerTime),
      version: packageJson.version
    });

    return next();
  }
}

function _getUpTime(then) {
  const milliseconds = moment(new Date(), 'DD/MM/YYYY HH:mm:ss').diff(moment(new Date(then), 'DD/MM/YYYY HH:mm:ss'));
  const day = moment.duration(milliseconds);
  return Math.floor(day.asHours()) + moment.utc(milliseconds).format(':mm:ss');
}

module.exports = HealthcheckController;