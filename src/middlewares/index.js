const auth = require('./auth-middleware');
const metrics = require('./metrics-middleware');
const requestCount = require('./request-count-middleware');
const requestDuration = require('./request-duration-middleware');
const repository = require('./repository-middleware');
const uptime = require('./uptime-middleware');

module.exports = {
  auth,
  metrics,
  requestCount,
  requestDuration,
  repository,
  uptime
};