const Prometheus = require('prom-client');

const totalErrors = new Prometheus.Counter({
  name: 'total_errors',
  help: 'Total number of errors',
  labelNames: ['payment_method']
});

const requestDuration = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 100, 300, 400, 500]
});


module.exports = (req, res, next) => {
  req.metrics = {
    totalErrors,
    requestDuration
  };

  res.startRequestTime = Date.now();

  next();
};
