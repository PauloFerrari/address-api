module.exports = (req, res, next) => {
  const responseTimeInMs = Date.now() - res.startRequestTime;
  req.metrics.requestDuration
    .labels(req.method, req.route.path, res.statusCode)
    .observe(responseTimeInMs);
  
  return next();
};
