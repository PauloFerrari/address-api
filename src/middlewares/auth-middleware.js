const constants = require('../constants');
const services = require('../services');
const logger = require('../logger');

module.exports = async (req, res, next) => {
  const token = req.headers['authorization'];

  if (token === undefined || token === '') {
    return res.status(401).json(constants.errors.token.invalid);
  }

  let client;

  try {
    client = services.authentication.verifyToken(token);
  } catch (error) {
    return res.status(401).json(constants.errors.token.invalid);
  }

  if (client.name === undefined || client.name === '') {
    return res.status(401).json(constants.errors.token.invalid);
  }

  try {
    const clientDb = await req.repository.findOne(constants.collections.clients, { name: client.name });

    if (clientDb === null) {
      return res.status(401).json(constants.errors.token.invalid);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json(constants.errors.database);
  }
  
  req.clientName = client.name;

  return next();
};