module.exports = (app, time) => {
  app.use((req, res, next) => {
    req.startServerTime = time;
    next();
  });
};