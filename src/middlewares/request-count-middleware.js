module.exports = (app, requestCount) => {
  app.use((req, res, next) => {
    requestCount++;
    req.requestCount = requestCount;
    next();
  });
};