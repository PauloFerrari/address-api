const config = require('../config');
const constants = require('../constants');
const logger = require('../logger');
const services = require('../services');

module.exports = async (req, res, next) => {
  try {
    const database = await services.database.getConnection(config.database.name);

    req.repository = new services.Repository(database);  
  } catch (error) {
    logger.error(error);
    return res.status(500).json(constants.errors.database);
  }

  return next();
};
