module.exports = {
  host: process.env.ADDRESS_API_HOST || '127.0.0.1',
  port: process.env.ADDRESS_API_PORT || 8080,
  logLevel: process.env.LOG_LEVEL || 'info',
  token: {
    secret: process.env.API_SECRET || 'apDdkpa/*/3w123',
    expirationTime: process.env.EXPIRATION_TIME || 300,
  },

  database: {
    host: process.env.MONGO_HOST || '127.0.0.1',
    name: process.env.MONGO_DATABASE_NAME || 'dev',
    port: process.env.MONGO_PORT || '27017'
  }
};