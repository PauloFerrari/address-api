const router = require('express').Router();
const controllers = require('../controllers');
const middlewares = require('../middlewares');
const services = require('../services');

// Healthcheck
services.router.create(router, 'GET', '/v1/healthcheck', controllers.healthcheck.get);

// Token
services.router.create(router, 'GET', '/v1/token', controllers.authentication.verifyToken, middlewares.auth);
services.router.create(router, 'POST', '/v1/token', controllers.authentication.generateToken);

// Address
services.router.create(router, 'GET', '/v1/address/:zipCode', controllers.address.getByZipCode, middlewares.auth);

module.exports = router;