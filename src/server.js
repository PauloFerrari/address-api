const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Prometheus = require('prom-client');
const metricsInterval = Prometheus.collectDefaultMetrics();

const config = require('./config');
const logger = require('./logger');
const middlewares = require('./middlewares');
const v1 = require('./routes/v1');

const app = express();
app.use(bodyParser.json());
app.use(cors());

let uptime = Date.now();
let requestCount = 0;

app.use(middlewares.metrics);
middlewares.uptime(app, uptime);
middlewares.requestCount(app, requestCount);
app.use(middlewares.repository);

app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType);
  res.end(Prometheus.register.metrics());
});

app.use(v1);

app.use(middlewares.requestDuration);

const server = app.listen(config.port, () => logger.info(`Address API Listing port: ${config.port}`));

process.on('SIGTERM', () => {
  clearInterval(metricsInterval);

  server.close((err) => {
    if (err) {
      logger.error(err);
      process.exit(1);
    }

    process.exit(0);
  });
});