const authentication = require('./authentication-service');
const database = require('./database-service');
const error = require('./error-service');
const Repository = require('./repository-service');
const router = require('./router-service');

module.exports = {
  authentication,
  database,
  error,
  Repository,
  router
};
