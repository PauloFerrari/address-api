const { DatabaseError } = require('../errors');
const logger = require('../logger');

class Repository {
  constructor(database) {
    this.database = database;
  }

  async findOne(collection, filter, options) {
    try {
      return await this.database.collection(collection).findOne(filter, options);
    } catch (error) {
      logger.error(error);
      throw new DatabaseError();
    }
  }

  async findAddressByZipCode(collection, zipCodes) {
    try {
      const result = await this.database.collection(collection).find({ $or: zipCodes}).sort({zipCode: -1}).limit(1).toArray();

      if (result.length === 0) return null;

      return result[0];
    } catch (error) {
      logger.error(error);
      throw new DatabaseError();
    }
  }
}


module.exports = Repository;