'use strict';

const { DatabaseError, NotFoundError, ServiceUnavailableError, Unauthorized, ValidationError } = require('../errors');
const constants = require('../constants');
const logger = require('../logger');

class ErrorService {
  static async verifyError(req, res, next, controller) {
    try {
      await controller(req, res, next);
    } catch (error) {
      return _errorResponse(req, res, next, error);
    }
  }
}

const _errorResponse = (req, res, next, error) => {
  switch (error.constructor) {
    case DatabaseError:
    case ServiceUnavailableError:
      res.status(error.code).send(JSON.parse(error.message));
      req.metrics.totalErrors.inc();
      break;
    case NotFoundError:
    case Unauthorized:
    case ValidationError:
      res.status(error.code).send(JSON.parse(error.message));
      break;
    default:
      logger.error(error);
      req.metrics.totalErrors.inc();
      res.status(500).send(constants.errors.internalServerError);
  }

  return next();
};

module.exports = ErrorService;
