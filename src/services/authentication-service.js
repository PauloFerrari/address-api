const jwt = require('jsonwebtoken');
const config = require('../config');

class AuthenticationService {

  static genareteToken(data) {
    return jwt.sign(data, config.token.secret, {
      expiresIn: config.token.expirationTime,
      algorithm: 'HS256'
    });
  }

  static verifyToken(token) {
    return jwt.verify(token, config.token.secret);
  }
}

module.exports = AuthenticationService;