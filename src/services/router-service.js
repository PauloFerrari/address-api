const error = require('./error-service');

class RouterService {
  static async create(router, verb, path, controller, ...middlewares) {
    if (verb === 'GET') {
      router.route(path).get(...middlewares, (req, res, next) => error.verifyError(req, res, next, controller));
      return;
    }

    if (verb === 'POST') {
      router.route(path).post(...middlewares, (req, res, next) => error.verifyError(req, res, next, controller));
      return;
    }

    if (verb === 'PUT') {
      router.route(path).put(...middlewares, (req, res, next) => error.verifyError(req, res, next, controller));
      return;
    }

    if (verb === 'PATCH') {
      router.route(path).patch(...middlewares, (req, res, next) => error.verifyError(req, res, next, controller));
      return;
    }

    if (verb === 'DELETE') {
      router.route(path).delete(...middlewares, (req, res, next) => error.verifyError(req, res, next, controller));
    }
  }
}

module.exports = RouterService;
