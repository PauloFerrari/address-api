const config = require('../config');
const { MongoClient } = require('mongodb');

class DatabaseService {
  static async getConnection(dbName) {
    this.dbConnection = await _connect(this.dbConnection);

    return this.dbConnection.db(dbName);
  }
}

const _connect = async (dbConnection) => {
  if (dbConnection && dbConnection.isConnected()) {
    return dbConnection;
  }

  const connection = `mongodb://${config.database.host}:${config.database.port}`;

  dbConnection = await MongoClient.connect(
    connection,
    {
      native_parser: true,
      useUnifiedTopology: true,
      useNewUrlParser: true
    }
  );

  return dbConnection;
};

module.exports = DatabaseService;