const collections = require('./collections-constants');
const errors = require('./errors-constants');

module.exports = {
  collections,
  errors
};