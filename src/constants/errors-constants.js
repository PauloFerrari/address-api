module.exports = {
  database: {
    errorCode: 'DATABASE_ERROR',
    errorMessage: 'Erro no banco de dados'
  },
  internalServerError: {
    errorCode: 'INTERNAL_SERVER_ERROR',
    errorMessage: 'Erro interno'
  },
  serviceUnavailable: {
    errorCode: 'SERVICE_UNAVAILABLE',
    errorMessage: 'Serviço indisponível'
  },
  unauthorized: {
    errorCode: 'UNAUTHORIZED',
    errorMessage: 'Não autorizado'
  },
  parameters: {
    invalid: {
      errorCode: 'PARAMETERS_INVALID',
      errorMessage: 'Parâmetros inválidos'
    }
  },
  name: {
    required: {
      errorCode: 'CLIENT_NAME_IS_REQUIRED',
      errorMessage: 'Nome é obrigatório'
    }
  },
  secret: {
    required: {
      errorCode: 'SECRET_IS_REQUIRED',
      errorMessage: 'Secret é obrigatório'
    }
  },
  token: {
    invalid: {
      errorCode: 'TOKEN_INVALID', 
      errorMessage: 'Token inválido'
    }
  },
  zipCode: {
    invalid: {
      errorCode: 'ZIP_CODE_INVALID', 
      errorMessage: 'Cep inválido'
    },
    notFound: {
      errorCode: 'ZIP_CODE_NOT_FOUND',
      errorMessage: 'Cep não encontrado'
    }
  }
};
