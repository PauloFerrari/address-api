const authentication = require('./authentication');
const address = require('./address');

module.exports = {
  authentication,
  address
};
