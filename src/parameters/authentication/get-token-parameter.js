const constants = require('../../constants');

class GetTokenParameter {
  constructor(req) {
    this.client = {};
    this.client.name = req.body.name;
    this.client.secret = req.body.secret;
  }

  validate() {
    let errors = [];

    if (this.client.name === undefined) {
      errors.push(constants.errors.name.required);
    }

    if (this.client.secret === undefined) {
      errors.push(constants.errors.secret.required);
    }

    return errors;
  }

}

module.exports = GetTokenParameter;