const constants = require('../../constants');

class GetAddressParameter {
  constructor(req) {
    let zipCode = req.params.zipCode;

    if (zipCode === undefined || !_isOnlyNumber(zipCode) || zipCode.length !== 8) return;

    if (isNaN(parseInt(zipCode))) return;

    this.zipCodes = _buildFindZipCode(zipCode);
  }

  validate() {
    let errors = [];

    if (this.zipCodes === undefined) {
      errors.push(constants.errors.zipCode.invalid);
    }

    return errors;
  }
}

const _isOnlyNumber = (value) => /^\d+$/.test(value);

const _buildFindZipCode = (zipCode) => {
  const result = [];
  zipCode = _removeZeroInTheLeft(zipCode);
  const zipCodeLength = zipCode.length - 1;
  let hasUpdate = false;

  for (let index = zipCodeLength; index > 0; index--) {
    if (zipCode[index] === '0') {
      if (zipCode[index - 1] !== '0' && !hasUpdate) {
        result.push({ zipCode: parseInt(zipCode) });
      }

      continue;
    }
    
    if (zipCodeLength === index) {
      result.push({ zipCode: parseInt(zipCode) });
    }

    zipCode = zipCode.substring(0, index) + '0' + zipCode.substring(index + 1);

    hasUpdate = true;

    result.push({ zipCode: parseInt(zipCode) });
  }

  return result;
};

const _removeZeroInTheLeft = (value) => parseInt(value).toString();

module.exports = GetAddressParameter;