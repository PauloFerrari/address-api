const BaseError = require('./base-error');
const constants = require('../constants');

class ValidationError extends BaseError {
  constructor(errorDetail) {
    const message = constants.errors.parameters.invalid;

    if (errorDetail) {
      message.innerErrors = errorDetail;
    }

    super(400, JSON.stringify(message));
  }
}

module.exports = ValidationError;
