const DatabaseError = require('./database-error');
const NotFoundError = require('./not-found-error');
const ServiceUnavailableError = require('./service-unavailable');
const ValidationError = require('./validation-error');
const Unauthorized = require('./unauthorized-error');

module.exports = {
  DatabaseError,
  NotFoundError,
  ServiceUnavailableError,
  ValidationError,
  Unauthorized
};
