const BaseError = require('./base-error');
const constants = require('../constants');

class DatabaseError extends BaseError {
  constructor() {
    super(500, JSON.stringify(constants.errors.database));
  }
}

module.exports = DatabaseError;
