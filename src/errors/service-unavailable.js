const BaseError = require('./base-error');
const constants = require('../constants');

class ServiceUnavailableError extends BaseError {
  constructor() {
    super(503, JSON.stringify(constants.errors.serviceUnavailable));
  }
}

module.exports = ServiceUnavailableError;
