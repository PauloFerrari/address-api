const BaseError = require('./base-error');
const contants = require('../constants');

class UnauthorizedError extends BaseError {
  constructor() {
    super(401, JSON.stringify(contants.errors.unauthorized));
  }
}

module.exports = UnauthorizedError;
