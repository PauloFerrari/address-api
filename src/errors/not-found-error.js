const BaseError = require('./base-error');

class NotFoundError extends BaseError {
  constructor(type) {
    super(404, JSON.stringify(type));
  }
}

module.exports = NotFoundError;
