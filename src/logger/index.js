const winston = require('winston');
const config = require('../config');
const packageJson = require('../../package.json');

const _errorStackFormat = winston.format(info => {
  if (info instanceof Error) {
    return Object.assign({}, info, {
      stack: info.stack,
      message: info.message
    });
  }
  return info;
});

const logger = winston.createLogger({
  level: config.logLevel,
  defaultMeta: {
    service: packageJson.name,
    version: packageJson.version
  },
  format: winston.format.combine(
    _errorStackFormat(),
    winston.format.splat(),
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: './tmp/all-logs.log' }),
    new winston.transports.Console()
  ],
});

module.exports = logger;