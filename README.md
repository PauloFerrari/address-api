
# Address API

#### Serviço de busca de cep.

### Índice

* [1. Escolha da tecnologia](#markdown-header-1-escolha-da-tecnologia)
* [2. Requisitos](#markdown-header-2-estrategia)
* [3. Padrão e arquitetura](#markdown-header-3-padrao-e-arquitetura)
* [4. Swagger](#markdown-header-4-swagger)
* [5. Requisitos](#markdown-header-5-requisitos)
* [6. Instalação](#markdown-header-6-instalacao)
* [7. Váriavies de ambiente](#markdown-header-7-variaveis-de-ambiente)
* [8. Rodar Aplicação](#markdown-header-8-rodar-aplicacao)
* [9. Usando Docker Compose](#markdown-header-9-usando-docker-compose)
* [10. Ambiente de desenvolvimento](#markdown-header-10-ambiente-de-desenvolvimento)
* [11. Testes](#markdown-header-11-testes)
* [12. Prometheus](#markdown-header-12-prometheus)
* [13. MongoDB](#markdown-header-13-mongodb)


### 1. Escolha da tecnologia

Escolhi Node.js pois tenho mais familiaridade com a linguagem e mongodb pois ele é NOSQL e uma grande vantagem é que se o banco de dados não estiver aguentando as requisições podemos criar réplicas desse banco para melhorar a performance, diferente do banco relacional que temos que aumentar o hardware da máquina.

### 2. Estratégia

Para resolver o problema de substituir o zero da esquerda, eu não queria executar várias busca no banco de dados, pois não é performático, o que fiz foi dentro da aplicação pegar o cep do request e criar um array com os ceps já colocando o zero a esquerda, exemplo:
```
cep request: 15085370
array da busca: [15085370, 15085300, 15085000, 15080000, 15000000, 10000000]
```
Para executar essa busca no mongo utilizei o find, sort e limit, faço a busca colocando $or, ordeno pelo cep e limito a 1 registro, assim terei o resultado com apenas uma consulta no banco.
 
Olhando ainda para performance criei um index na collection address no campo zipCode para otimizar a busca e também utilizei somente a lib do mongo-drive para conexão com o banco.
 
Na autenticação utilizei jwt, tenho dois campos no banco de dados para a autenticação, o name e secret, com esses dados mais uma secret que fica em uma variável de ambiente da aplicação faço a autenticação da API.
 
Já para subir a aplicação e mongo utilizei docker-compose, assim conseguia fazer teste de forma fácil e isolar a API em um container.
 
Na parte de métricas, criei uma rota para o prometheus /metrics que expõe as métricas padrões e mais duas, uma é o tempo de cada request(histogram) e o total de erros(counter), para conseguir ver um gráfico dessas métricas no ambiente de desenvolvimento dentro do comando "npm run compose-up" eu subo um container do prometheus, ele irá pegar as métricas da api de 5 em 5 segundos e também tem a parte gráfica "http://localhost:9090".

### 3. Padrões e arquitetura

No código sempre tentei quebrá-lo em várias funções e classe para melhor entendimento e caso precisasse executar um teste unitário em um ponto específico.
 
A estrutura que criei na aplicação foi a seguinte:
 
**mongo-init**: banco de dados inicial.
**controller**: onde ficam as regras de negócio da aplicação.
**middlewares**: para usar antes de executar uma controller ou até mesmo depois, exemplo, a métrica de tempo do request.
**parameters**: que validam os dados da requisição e já manipula ele para o uso dentro da controller.
**repository**: acesso ao banco de dados, esse tentei colocar como injeção de dependência inserindo ele no req dentro de um middleware.
**constants**: para não ter strings espalhadas no código.
**errors**: dentro da aplicação queria criar erros tratados, para que dentro de uma controller ou até mesmo d repository eu desse um throw e uma classe tratasse esse erro e já respondesse para o client.
**services**: alguns services para ajudar dentro da aplicação, um importante é um wrapper do router do express para conseguir englobar a controller assim conseguindo tratar os erros criados.
**logger**: usado para logs estruturados.
**tests**: contém testes de integração e unitário.

### 4. Swagger

Dentro da pasta /docs contém o [swagger](/docs/swagger/swagger.yml) da aplicação.

### 5. Requisitos

* [Docker](https://www.docker.com)
* [Docker-compose](https://docs.docker.com/compose/install)
* [Node.js v12+](https://nodejs.org) 
* [Git](https://git-scm.com/)


### 6. Instalação

Clone do projeto `git clone https://bitbucket.org/PauloFerrari/address-api`.

Instalar dependencias `npm i`.

### 7. Variáveis de Ambiente

| Variável | Descrição |
| --- | ---|
| LOG_LEVEL | info,warn,error,fatal |
| ADDRESS_API_PORT | Porta da aplicação |
| API_SECRET | Secret da API |
| EXPIRATION_TIME | Tempo de expiração do token |
| MONGO_HOST | mongo host |
| MONGO_PORT | Porta do mongo  |
| MONGO_DATABASE_NAME | Nome do banco de dados |

### 8. Rodar aplicação

Para rodar a aplicação é necessário ter o mongoDB, execute o comando `npm run compose-up database` para iniciá-lo.

Rodar a aplicação `npm start`.

### 9. Usando Docker Compose

`npm run compose-up` irá subir o container da aplicação, mongoDB e prometheus.

Parar todos os containers `npm run compose-down`.

Rebuild dos containers `npm run compose-rebuild`, ele também tentará subir os containers novamente.

Executar e mostrar os logs dos containers `npm run compose-debug`.

### 10. Ambiente de desenvolvimento

Para rodar a aplicação com o modo DEV rode o comando `npm run dev`, ele irá subir a aplicação com o nodemon e irá atualizar ela em qualquer mudança do projeto.

### 11. Testes

Executar os testes `npm test`, esse comando precisa da aplicação e mongoDB, recomendo que roder o comando `npm run compose-up` antes.

Rodar o lint `npm run lint`.

### 12. Prometheus

Quando executar o comando `npm run compose-up`, irá subir um container de prometheus para que irá pegar as metricas da aplicação.

Exemplo: total de erros e tempo de resposta de cada request.

### 13. MongoDB

Quando executar o comando `npm run compose-up` ou `npm run compose-up database`, ambos iram subir um container de mongo que já teram dados iniciais, os dados estão na pasta [mongo-init](/mongo-init).
