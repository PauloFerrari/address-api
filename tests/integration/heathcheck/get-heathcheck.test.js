/* global describe,it */

const axios = require('axios');
const assert = require('assert');
const config = require('../../../src/config');
const packageJson = require('../../../package.json');

describe('verify status application /v1/healthcheck', function () {
  it('should return success', async function () {
    const result = await _requestHealthcheck();

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 200, 'result.status dont match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.mongo, true, 'result.data.mongo dont match');
    assert.notStrictEqual(result.data.requestCount, undefined, 'result.data.requestCount dont match');
    assert.notStrictEqual(result.data.uptime, undefined, 'result.data.uptime dont match');
    assert.strictEqual(result.data.version, packageJson.version, 'result.data.version dont match');
  });

  it('should return request counter', async function () {
    const resultOne = await _requestHealthcheck();

    assert.notStrictEqual(resultOne, undefined, 'result is undefined');
    assert.strictEqual(resultOne.status, 200, 'result.status dont match');
    assert.notStrictEqual(resultOne.data, undefined, 'result.data is undefined');
    assert.strictEqual(resultOne.data.mongo, true, 'result.data.mongo dont match');
    assert.notStrictEqual(resultOne.data.requestCount, undefined, 'result.data.requestCount dont match');
    assert.notStrictEqual(resultOne.data.uptime, undefined, 'result.data.uptime dont match');
    assert.strictEqual(resultOne.data.version, packageJson.version, 'result.data.version dont match');

    const resultSecond = await _requestHealthcheck();

    assert.notStrictEqual(resultSecond, undefined, 'result is undefined');
    assert.strictEqual(resultSecond.status, 200, 'result.status dont match');
    assert.notStrictEqual(resultSecond.data, undefined, 'result.data is undefined');
    assert.strictEqual(resultSecond.data.mongo, true, 'result.data.mongo dont match');
    assert.strictEqual(resultSecond.data.requestCount, resultOne.data.requestCount + 1, 'result.data.requestCount dont match');
    assert.notStrictEqual(resultSecond.data.uptime, undefined, 'result.data.uptime dont match');
    assert.strictEqual(resultSecond.data.version, packageJson.version, 'result.data.version dont match');
  });
});

const _requestHealthcheck = async () => {
  try {
    return await axios.get(`http://${config.host}:${config.port}/v1/healthcheck`);
  } catch (error) {
    return error.response;
  }
};
