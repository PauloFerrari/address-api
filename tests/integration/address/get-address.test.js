/* global describe,it, before, after */

const axios = require('axios');
const assert = require('assert');
const config = require('../../../src/config');
const services = require('../../../src/services');
const constants = require('../../../src/constants');
const mocks = require('../../mocks');

const CLIENT_NAME = 'test-integration';
const SECRET = 'secret';
let connection;

describe('Get address by zipCode /v1/address/:zipCode', function () {
  before(async function () {
    connection = await services.database.getConnection(config.database.name);

    await connection.collection(constants.collections.clients).insertOne({
      name: CLIENT_NAME,
      secret: SECRET
    });

    await connection.collection(constants.collections.address).insertMany(mocks.address);
  });

  after(async function () {
    await connection.collection(constants.collections.clients).deleteOne({ name: CLIENT_NAME });
    const zipCodes = mocks.address.map(item => item.zipCode);
    await connection.collection(constants.collections.address).deleteMany({ zipCode: { $in: zipCodes }});
  });

  it('should return address', async function () {
    const addressMock = mocks.address[0];
    const result = await _requestGetAddressByZipCode(addressMock.zipCode);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 200, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.notStrictEqual(result.data._id, undefined, 'result.data._id doent match');
    assert.strictEqual(result.data.zipCode, addressMock.zipCode, 'result.data.zipCode doent match');
    assert.strictEqual(result.data.address, addressMock.address, 'result.data.address doent match');
    assert.strictEqual(result.data.neighborhood, addressMock.neighborhood, 'result.data.neighborhood doent match');
    assert.strictEqual(result.data.city, addressMock.city, 'result.data.city doent match');
    assert.strictEqual(result.data.state, addressMock.state, 'result.data.state doent match');
  });

  it('should return address when needs to change the zip code code by placing zero on the right', async function () {
    const zipCodeFind = mocks.address[0];
    const addressMock = mocks.address[1];
    const zipCode = zipCodeFind.zipCode - 1;
    const result = await _requestGetAddressByZipCode(zipCode);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 200, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.notStrictEqual(result.data._id, undefined, 'result.data._id doent match');
    assert.notStrictEqual(result.data.zipCode, zipCode, 'result.data.zipCode doent match');
    assert.strictEqual(result.data.address, addressMock.address, 'result.data.address doent match');
    assert.strictEqual(result.data.neighborhood, addressMock.neighborhood, 'result.data.neighborhood doent match');
    assert.strictEqual(result.data.city, addressMock.city, 'result.data.city doent match');
    assert.strictEqual(result.data.state, addressMock.state, 'result.data.state doent match');
  });

  it('should return token invalid', async function () {
    const addressMock = mocks.address[0];
    const result = await _requestGetAddressByZipCode(addressMock.zipCode, '');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.token.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.token.invalid.errorMessage, 'result.data.errorMessage dont match');
  });

  it('should return zip code not found', async function () {
    const result = await _requestGetAddressByZipCode(55555555);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 404, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.zipCode.notFound.errorCode, 'result.data.errorCode doent match');
    assert.strictEqual(result.data.errorMessage, constants.errors.zipCode.notFound.errorMessage, 'result.data.errorMessage doent match');
  });

  it('should return zip code invalid when send string', async function () {
    const result = await _requestGetAddressByZipCode('111zipecodeinvalid');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 400, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.parameters.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.parameters.invalid.errorMessage, 'result.data.errorMessage dont match');
    assert.notStrictEqual(result.data.innerErrors, undefined, 'result.data.innerErrors is undefined');
    assert.strictEqual(result.data.innerErrors.length, 1, 'result.data.innerErrors dont match');
    assert.strictEqual(result.data.innerErrors[0].errorCode, constants.errors.zipCode.invalid.errorCode, 'result.data.innerError[0].errorCode dont match');
    assert.strictEqual(result.data.innerErrors[0].errorMessage, constants.errors.zipCode.invalid.errorMessage, 'result.data.innerError[0].errorMessage dont match');
  });

  it('should return zip code invalid when send that longer than eight characters', async function () {
    const result = await _requestGetAddressByZipCode('150853711');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 400, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.parameters.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.parameters.invalid.errorMessage, 'result.data.errorMessage dont match');
    assert.notStrictEqual(result.data.innerErrors, undefined, 'result.data.innerErrors is undefined');
    assert.strictEqual(result.data.innerErrors.length, 1, 'result.data.innerErrors dont match');
    assert.strictEqual(result.data.innerErrors[0].errorCode, constants.errors.zipCode.invalid.errorCode, 'result.data.innerError[0].errorCode dont match');
    assert.strictEqual(result.data.innerErrors[0].errorMessage, constants.errors.zipCode.invalid.errorMessage, 'result.data.innerError[0].errorMessage dont match');
  });

  it('should return zip code invalid when send that less than eight characters', async function () {
    const result = await _requestGetAddressByZipCode('1508531');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 400, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.parameters.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.parameters.invalid.errorMessage, 'result.data.errorMessage dont match');
    assert.notStrictEqual(result.data.innerErrors, undefined, 'result.data.innerErrors is undefined');
    assert.strictEqual(result.data.innerErrors.length, 1, 'result.data.innerErrors dont match');
    assert.strictEqual(result.data.innerErrors[0].errorCode, constants.errors.zipCode.invalid.errorCode, 'result.data.innerError[0].errorCode dont match');
    assert.strictEqual(result.data.innerErrors[0].errorMessage, constants.errors.zipCode.invalid.errorMessage, 'result.data.innerError[0].errorMessage dont match');
  });
});

const _getToken = () => {
  return services.authentication.genareteToken({
    name: CLIENT_NAME,
    secret: SECRET
  });
};

const _requestGetAddressByZipCode = async (zipCode, token) => {
  if (token === undefined) {
    token = _getToken();
  }

  try {
    return await axios.get(`http://${config.host}:${config.port}/v1/address/${zipCode}`, {
      headers: {
        'Authorization': token
      }
    });
  } catch (error) {
    return error.response;
  }
};
