/* global describe,it, before, afterEach */

const axios = require('axios');
const assert = require('assert');
const config = require('../../../src/config');
const services = require('../../../src/services');
const constants = require('../../../src/constants');

const CLIENT_NAME = 'test-integration';
const SECRET = 'secret';
let connection;

describe('Verify token /v1/token', function () {
  before(async function () {
    connection = await services.database.getConnection(config.database.name);
  });

  afterEach(async function () {
    await connection.collection(constants.collections.clients).deleteMany({name: CLIENT_NAME});
  });

  it('should return no content', async function () {
    await connection.collection(constants.collections.clients).insertOne({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const token = services.authentication.genareteToken({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const result = await _requestVerifyToken(token);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 204, 'result.status doent match');
  });

  it('should return token invalid when doenst send the token on header', async function () {
    await connection.collection(constants.collections.clients).insertOne({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const result = await _requestVerifyToken('');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.token.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.token.invalid.errorMessage, 'result.data.errorMessage dont match');
  });

  it('should return token invalid when send token invalid', async function () {
    await connection.collection(constants.collections.clients).insertOne({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const token = services.authentication.genareteToken({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const result = await _requestVerifyToken(token + 'invalid');

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.token.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.token.invalid.errorMessage, 'result.data.errorMessage dont match');
  });

  it('should return token invalid when send client invalid in token', async function () {
    const token = services.authentication.genareteToken({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const result = await _requestVerifyToken(token);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.token.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.token.invalid.errorMessage, 'result.data.errorMessage dont match');
  });

  it('should return token invalid when send token without client name', async function () {
    const token = services.authentication.genareteToken({
      secret: SECRET
    });

    const result = await _requestVerifyToken(token);

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status doent match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.token.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.token.invalid.errorMessage, 'result.data.errorMessage dont match');
  });
});

const _requestVerifyToken = async (token) => {
  try {
    return await axios.get(`http://${config.host}:${config.port}/v1/token`, {
      headers: {
        'Authorization': token
      }
    });
  } catch (error) {
    return error.response;
  }
};
