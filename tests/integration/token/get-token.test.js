/* global describe,it, before, afterEach */

const axios = require('axios');
const assert = require('assert');
const config = require('../../../src/config');
const services = require('../../../src/services');
const constants = require('../../../src/constants');

const CLIENT_NAME = 'test-integration';
const SECRET = 'secret-integration';
let connection;

describe('Get token /v1/token', function () {
  before(async function () {
    connection = await services.database.getConnection(config.database.name);
  });

  afterEach(async function () {
    await connection.collection(constants.collections.clients).deleteOne({name: CLIENT_NAME});
  });

  it('should return token', async function () {
    await connection.collection(constants.collections.clients).insertOne({
      name: CLIENT_NAME,
      secret: SECRET
    });

    const result = await _requestToken({
      name: CLIENT_NAME,
      secret: SECRET
    });

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 200, 'result.status dont match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.notStrictEqual(result.data.token, undefined, 'result.data.token dont match');
  });

  it('should return unauthorized', async function () {
    const result = await _requestToken({
      name: CLIENT_NAME,
      secret: SECRET
    });

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 401, 'result.status dont match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.unauthorized.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.unauthorized.errorMessage, 'result.data.errorMessage dont match');
  });

  it('should return all field required', async function () {
    const result = await _requestToken();

    assert.notStrictEqual(result, undefined, 'result is undefined');
    assert.strictEqual(result.status, 400, 'result.status dont match');
    assert.notStrictEqual(result.data, undefined, 'result.data is undefined');
    assert.strictEqual(result.data.errorCode, constants.errors.parameters.invalid.errorCode, 'result.data.errorCode dont match');
    assert.strictEqual(result.data.errorMessage, constants.errors.parameters.invalid.errorMessage, 'result.data.errorMessage dont match');
    assert.notStrictEqual(result.data.innerErrors, undefined, 'result.data.innerErrors is undefined');
    assert.strictEqual(result.data.innerErrors.length, 2, 'result.data.innerErrors dont match');
    assert.strictEqual(result.data.innerErrors[0].errorCode, constants.errors.name.required.errorCode, 'result.data.innerError[0].errorCode dont match');
    assert.strictEqual(result.data.innerErrors[0].errorMessage, constants.errors.name.required.errorMessage, 'result.data.innerError[0].errorMessage dont match');
    assert.strictEqual(result.data.innerErrors[1].errorCode, constants.errors.secret.required.errorCode, 'result.data.innerError[1].errorCode dont match');
    assert.strictEqual(result.data.innerErrors[1].errorMessage, constants.errors.secret.required.errorMessage, 'result.data.innerError[1].errorMessage dont match');
  });
});

const _requestToken = async (data) => {
  try {
    return await axios.post(`http://${config.host}:${config.port}/v1/token`, data);
  } catch (error) {
    return error.response;
  }
};
