/* global describe,it */

const assert = require('assert');
const GetAddressParameters = require('../../../src/parameters/address/get-address-parameters');

describe('Get address parameters', function () {
  it('should return zip codes', async function () {
    const req = {
      params: {
        zipCode: '15881476'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 8, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 15881476, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 15881470, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 15881400, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 15881000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 15880000, 'params.zipCodes[4].zipCode doent match');
    assert.strictEqual(params.zipCodes[5].zipCode, 15800000, 'params.zipCodes[5].zipCode doent match');
    assert.strictEqual(params.zipCodes[6].zipCode, 15000000, 'params.zipCodes[6].zipCode doent match');
    assert.strictEqual(params.zipCodes[7].zipCode, 10000000, 'params.zipCodes[7].zipCode doent match');
  });

  it('should return zip codes when have one zero in the midle zip code', async function () {
    const req = {
      params: {
        zipCode: '15880476'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 7, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 15880476, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 15880470, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 15880400, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 15880000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 15800000, 'params.zipCodes[4].zipCode doent match');
    assert.strictEqual(params.zipCodes[5].zipCode, 15000000, 'params.zipCodes[5].zipCode doent match');
    assert.strictEqual(params.zipCodes[6].zipCode, 10000000, 'params.zipCodes[6].zipCode doent match');
  });

  it('should return zip codes when have zero in the rigth', async function () {
    const req = {
      params: {
        zipCode: '15881400'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 6, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 15881400, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 15881000, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 15880000, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 15800000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 15000000, 'params.zipCodes[4].zipCode doent match');
    assert.strictEqual(params.zipCodes[5].zipCode, 10000000, 'params.zipCodes[5].zipCode doent match');
  });
  

  it('should return zip codes when have zero in the rigth and in the middle', async function () {
    const req = {
      params: {
        zipCode: '15880400'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 5, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 15880400, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 15880000, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 15800000, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 15000000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 10000000, 'params.zipCodes[4].zipCode doent match');
  });


  it('should return zip codes when have zero in the left and rigth', async function () {
    const req = {
      params: {
        zipCode: '05881430'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 6, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 5881430, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 5881400, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 5881000, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 5880000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 5800000, 'params.zipCodes[4].zipCode doent match');
    assert.strictEqual(params.zipCodes[5].zipCode, 5000000, 'params.zipCodes[5].zipCode doent match');
  });

  it('should return zip codes when have zero in the left', async function () {
    const req = {
      params: {
        zipCode: '05881432'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 7, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 5881432, 'params.zipCodes[0].zipCode doent match');
    assert.strictEqual(params.zipCodes[1].zipCode, 5881430, 'params.zipCodes[1].zipCode doent match');
    assert.strictEqual(params.zipCodes[2].zipCode, 5881400, 'params.zipCodes[2].zipCode doent match');
    assert.strictEqual(params.zipCodes[3].zipCode, 5881000, 'params.zipCodes[3].zipCode doent match');
    assert.strictEqual(params.zipCodes[4].zipCode, 5880000, 'params.zipCodes[4].zipCode doent match');
    assert.strictEqual(params.zipCodes[5].zipCode, 5800000, 'params.zipCodes[5].zipCode doent match');
    assert.strictEqual(params.zipCodes[6].zipCode, 5000000, 'params.zipCodes[6].zipCode doent match');
  });

  it('should return one zip code when have zero in the left', async function () {
    const req = {
      params: {
        zipCode: '05000000'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 1, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 5000000, 'params.zipCodes[0].zipCode doent match');
  });

  it('should return one zip code', async function () {
    const req = {
      params: {
        zipCode: '10000000'
      }
    };

    const params = new GetAddressParameters(req);

    assert.strictEqual(params.zipCodes.length, 1, 'params.zipCodes.length doent match');
    assert.strictEqual(params.zipCodes[0].zipCode, 10000000, 'params.zipCodes[0].zipCode doent match');
  });
});